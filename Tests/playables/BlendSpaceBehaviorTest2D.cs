using NUnit.Framework;
using UnityEngine;
using static Monoid.Unity.Animation.BlendSpaceBehaviour2D;

namespace Monoid.Unity.Animation {

  [TestFixture]
  public class BlendSpaceBehaviourTest2D {

    [Test]
    public void TriangleInside() {

      var a = new Vector2(3.0f, 0.0f);
      var b = new Vector2(0.0f, 3.0f);
      var c = new Vector2(0.0f, 7.0f);

      // fully inside
      TriangleInsideOrders(a, b, c, new Vector2(0.1f, 3.5f));
      // on edge
      TriangleInsideOrders(a, b, c, new Vector2(0.0f, 3.5f));


      a = new Vector2(0.0f, 0.0f);
      b = new Vector2(0.0f, -3.0f);
      c = new Vector2(-3.0f, 0.0f);

      TriangleOutsideOrders(a, b, c, new Vector2(0.0f, 2.0f));

    }

    private void TriangleInsideOrders(Vector2 a, Vector2 b, Vector2 c, Vector2 v) {
      Assert.IsTrue(Triangle.Inside(a, b, c, v), $"Expected {v} to be inside Tri<{a},{b},{c}>");
      Assert.IsTrue(Triangle.Inside(b, a, c, v), $"Expected {v} to be inside Tri<{b},{a},{c}>");
      Assert.IsTrue(Triangle.Inside(a, c, b, v), $"Expected {v} to be inside Tri<{a},{c},{b}>");
      Assert.IsTrue(Triangle.Inside(c, a, b, v), $"Expected {v} to be inside Tri<{c},{a},{b}>");
      Assert.IsTrue(Triangle.Inside(b, c, a, v), $"Expected {v} to be inside Tri<{b},{c},{a}>");
      Assert.IsTrue(Triangle.Inside(c, b, a, v), $"Expected {v} to be inside Tri<{c},{b},{a}>");
    }

    private void TriangleOutsideOrders(Vector2 a, Vector2 b, Vector2 c, Vector2 v) {
      Assert.IsFalse(Triangle.Inside(a, b, c, v), $"Expected {v} to be outside Tri<{a},{b},{c}>");
      Assert.IsFalse(Triangle.Inside(b, a, c, v), $"Expected {v} to be outside Tri<{b},{a},{c}>");
      Assert.IsFalse(Triangle.Inside(a, c, b, v), $"Expected {v} to be outside Tri<{a},{c},{b}>");
      Assert.IsFalse(Triangle.Inside(c, a, b, v), $"Expected {v} to be outside Tri<{c},{a},{b}>");
      Assert.IsFalse(Triangle.Inside(b, c, a, v), $"Expected {v} to be outside Tri<{b},{c},{a}>");
      Assert.IsFalse(Triangle.Inside(c, b, a, v), $"Expected {v} to be outside Tri<{c},{b},{a}>");
    }


  }

}