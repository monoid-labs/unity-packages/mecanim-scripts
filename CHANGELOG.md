# Changelog

All notable changes to this package will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [1.0.19] - 2022-04-06

- AnimationStatesBehaviour fix for default state blending

## [1.0.18] - 2022-03-17

- Remove obsolete `normalizeWeights` argument for `AnimationMixerPlayable` to avoid warnings in newer Unity versions

## [1.0.17] - 2022-03-15

- AnimationStatesBehaviour weight property fix for default state blending

## [1.0.16] - 2021-08-04

- AnimationStatesBehaviour weight property re-added

## [1.0.15] - 2021-07-08

- AnimationStatesBehaviour reworked (2nd iteration; dynamic input count instead of Null Playables)

## [1.0.14] - 2021-07-08

- AnimationStatesBehaviour reworked (cleaner code, fix animation events, performance optimizations)

## [1.0.13] - 2021-06-30

- Fix AnimationStatesBehaviour triggering animation events twice when blending (2nd revision)

## [1.0.12] - 2021-06-28

- Fix AnimationStatesBehaviour transitions with time scaling support

## [1.0.11] - 2021-06-28

- Fix AnimationStatesBehaviour triggering animation events twice when blending

## [1.0.10] - 2021-04-05

- AnimatorControllerEditorUtility: Apply overrides to controller

## [1.0.9] - 2021-03-02

- BlendSpaces without assets

## [1.0.8] - 2021-01-20

- Texture Names

## [1.0.7] - 2020-11-20

- FIX: Action State Weight

## [1.0.6] - 2020-11-13

- Proper Action State Weight / Transition Blending for non-states

## [1.0.5] - 2020-07-12

- Blend-Spaces with speed multipliers
- FIX: BlendSpace2D interpolation without zero position

## [1.0.4] - 2020-06-28

- BSD 2-Clause License
- `.editorconfig`

## [1.0.3] - 2020-04-21

- Blend-Space for time syncing support (if time propagation is enabled)

## [1.0.2] - 2020-04-19

- More transition blending fixes
- Improved transition interruption (use current or next as new current based on the transition progress)
- State-based Animation Sample

## [1.0.1] - 2020-02-24

Fixed transition blending

## [1.0.0] - 2019-12-17

Proper cycles matching for 1D & 2D Blend-Spaces

## [0.0.1-preview.1] - 2018-09-10

This is the first release.
