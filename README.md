# Animation Scripts for Unity3D

Scripts related to _Unity3D_'s animation system / the `Animator`.

- Allows for an easily creation of a state machines via code

## License

BSD 2-Clause License (see [LICENSE](LICENSE.md))
