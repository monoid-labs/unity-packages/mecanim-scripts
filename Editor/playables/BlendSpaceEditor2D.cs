using UnityEditor;
using UnityEngine;

namespace Monoid.Unity.Animation {

  [CustomEditor(typeof(BlendSpace2D))]
  public class BlendSpaceEditor2D : Editor {

    // Based on: Unity's BlendTreeEditor's 2D case
    // https://github.com/Unity-Technologies/UnityCsReference/blob/54dd67f09e090b1ad5ba1f55886f327106406b0c/Editor/Mono/Inspector/BlendTreeInspector.cs

    static class Styles {

      public static readonly GUIContent Interpolate = new GUIContent("Interpolate");

      public static readonly Color PositionSelectedColor = new Color(1.0f, 0.35f, 0.25f);
      public static readonly Color PositionShapeColor = new Color(0.5f, 0.7f, 1.0f);
      public static readonly Color PositionLineColor = !EditorGUIUtility.isProSkin ? new Color(0.0f, 0.0f, 0.0f, 1.0f) : new Color(1.0f, 1.0f, 1.0f, 1.0f);

      public static readonly GUIStyle HeaderIcon = new GUIStyle();
      public static readonly GUIContent SpeedIcon = new GUIContent(EditorGUIUtility.IconContent("SpeedScale"));

      public static readonly GUIContent Clip = new GUIContent("Clip");
      public static readonly GUIContent PositionX = new GUIContent("Pos X");
      public static readonly GUIContent PositionY = new GUIContent("Pos Y");
      public static readonly GUIContent Speed = new GUIContent("Speed");
      public static readonly GUIContent BlendSpace = new GUIContent("Blend Space");
    }

    const int BlendTextureSize = 256;

    static readonly int BlendAnimationID = "BlendAnimationIDHash".GetHashCode();

    SerializedProperty children;
    UnityEditorInternal.ReorderableList reorderableList;
    Texture2D blendTexture;

    void OnDestroy() {
      if (blendTexture) {
        DestroyImmediate(blendTexture);
      }
    }

    public override void OnInspectorGUI() {
      if (children == null) {
        children = serializedObject.FindProperty("states");
        reorderableList = new UnityEditorInternal.ReorderableList(serializedObject, children) {
          drawHeaderCallback = ReorderableListHeader,
          drawElementCallback = ReorderableListElement,
          onAddCallback = ReorderableListElementAdd,
          onRemoveCallback = ReorderableListElementRemove
        };
      }

      if (!blendTexture) {
        blendTexture = new Texture2D(BlendTextureSize, BlendTextureSize, TextureFormat.RGBA32, false) {
          name = nameof(BlendSpaceEditor2D),
          hideFlags = HideFlags.HideAndDontSave,
          wrapMode = TextureWrapMode.Clamp,
          filterMode = FilterMode.Bilinear
        };
      }

      GUI.enabled = false;
      EditorGUILayout.PropertyField(serializedObject.FindProperty("m_Script"));
      GUI.enabled = true;

      EditorGUILayout.Space();
      var space = target as BlendSpace2D;
      EditorGUILayout.LabelField(Styles.BlendSpace, EditorStyles.boldLabel);
      if (children.arraySize > 0) {
        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        BlendGraph(space, GUILayoutUtility.GetAspectRect(1, GUILayout.MaxWidth(256)));
        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();
      }

      EditorGUILayout.Space();
      reorderableList.DoLayoutList();

      EditorGUILayout.Space();

      EditorGUI.BeginChangeCheck();
      EditorGUILayout.PropertyField(serializedObject.FindProperty("interpolate"), Styles.Interpolate);
      if (EditorGUI.EndChangeCheck()) {
        serializedObject.ApplyModifiedProperties();
      }
    }

    void ReorderableListElementAdd(UnityEditorInternal.ReorderableList list) {
      var index = children.arraySize++;
      var child = children.GetArrayElementAtIndex(index);
      var speed = child.FindPropertyRelative("speed");
      speed.floatValue = 1.0f;
      serializedObject.ApplyModifiedProperties();
      //UnityEditorInternal.ReorderableList.defaultBehaviours.DoAddButton(list);
    }
    void ReorderableListElementRemove(UnityEditorInternal.ReorderableList list) {
      for (int i = list.index + 1, n = children.arraySize; i < n; i++) {
        children.MoveArrayElement(i, i - 1);
      }
      children.arraySize--;
      serializedObject.ApplyModifiedProperties();
      //UnityEditorInternal.ReorderableList.defaultBehaviours.DoRemoveButton(list);
    }

    void ReorderableListHeader(Rect rect) {
      var clip_rect = rect.SliceX(rect.width * 0.6f, 4.0f);
      GUI.Label(clip_rect, Styles.Clip, EditorStyles.label);

      var position_x_rect = rect.SliceX(rect.width / 3, 4.0f);
      GUI.Label(position_x_rect, Styles.PositionX, EditorStyles.label);
      var position_y_rect = rect.SliceX(rect.width / 2, 4.0f);
      GUI.Label(position_y_rect, Styles.PositionY, EditorStyles.label);

      var speed_rect = rect.SliceX(rect.width, 4.0f);
      GUI.Label(speed_rect, Styles.Speed, EditorStyles.label);
    }

    void ReorderableListElement(Rect rect, int index, bool isActive, bool isFocused) {
      rect.y++;
      rect.height = 16.0f;

      var child = children.GetArrayElementAtIndex(index);
      var clip = child.FindPropertyRelative("clip");
      var position = child.FindPropertyRelative("position");
      var speed = child.FindPropertyRelative("speed");

      var clip_rect = rect.SliceX(rect.width * 0.6f, 4.0f);
      EditorGUI.PropertyField(clip_rect, clip, GUIContent.none);

      var pos = position.vector2Value;
      var position_x_rect = rect.SliceX(rect.width / 3, 4.0f);
      pos.x = EditorGUI.DelayedFloatField(position_x_rect, pos.x);
      var position_y_rect = rect.SliceX(rect.width / 2, 4.0f);
      pos.y = EditorGUI.DelayedFloatField(position_y_rect, pos.y);
      position.vector2Value = pos;

      var speed_rect = rect.SliceX(rect.width, 4.0f);
      EditorGUI.PropertyField(speed_rect, speed, GUIContent.none);

      serializedObject.ApplyModifiedProperties();
    }

    void BlendGraph(BlendSpace2D space, Rect area) {
      var n = children.arraySize;
      var positions = new Vector2[n];
      var min = Vector2.one * float.PositiveInfinity;
      var max = Vector2.one * float.NegativeInfinity;
      for (int i = 0; i < n; i++) {
        var child = children.GetArrayElementAtIndex(i);
        var position = child.FindPropertyRelative("position");
        positions[i] = position.vector2Value;
        min = Vector2.Min(min, positions[i]);
        max = Vector2.Max(max, positions[i]);
      }

      var triangles = BlendSpaceBehaviour2D.Triangulate(positions);

#if false // Pivot Center
      var center = 0.5f * (min + max);
      var extent = 0.5f * (max - min);
      extent = Vector2.one * Mathf.Max (extent.x, extent.y);
#else     // Zero Center
      var center = Vector2.zero;
      var extent = Vector2.one * Mathf.Max(min.magnitude, max.magnitude);
#endif
      extent *= 1.125f;
      min = center - extent;
      max = center + extent;

      // convert to UI space
      for (int i = 0; i < positions.Length; i++) {
        positions[i] = (positions[i] - min) / (max - min); // to [0,1]
        positions[i].y = 1.0f - positions[i].y;
        positions[i] = positions[i] * area.size + area.min; // to [area]
        positions[i].x = Mathf.Round(positions[i].x);
        positions[i].y = Mathf.Round(positions[i].y);
        positions[i] += new Vector2(0.5f, 0.5f);
      }

      var controlID = GUIUtility.GetControlID(BlendAnimationID, FocusType.Passive);

      var evt = Event.current;
      switch (evt.GetTypeForControl(controlID)) {
        case EventType.Repaint:
          EditorGUI.DrawRect(area, new Color(1.0f, 1.0f, 1.0f, 0.35f));

          var color = Handles.color;
          Handles.color = Color.black;
          Handles.DrawLine(area.min, area.min + new Vector2(area.width, 0.0f));
          Handles.DrawLine(area.min, area.min + new Vector2(0.0f, area.height));
          Handles.DrawLine(area.max, area.max - new Vector2(area.width, 0.0f));
          Handles.DrawLine(area.max, area.max - new Vector2(0.0f, area.height));

          if (triangles.Length > 0 && reorderableList.index >= 0) {
            var selected = reorderableList.index;
            for (int y = 0; y < BlendTextureSize; y++) {
              for (int x = 0; x < BlendTextureSize; x++) {
                var c = new Color();

                var sample = area.min + area.size * new Vector2(x, BlendTextureSize - y - 1) / (float)BlendTextureSize;
                foreach (var tri in triangles) {
                  if (tri.a != selected && tri.b != selected && tri.c != selected) {
                    continue;
                  }
                  if (tri.Inside(positions, sample)) {
                    var weights = tri.Weights(positions, sample);
                    var t = (tri.a == selected) ? weights.x
                          : (tri.b == selected) ? weights.y
                          : (tri.c == selected) ? weights.z
                          : 0.0f;
                    c = new Color(0.25f, 0.5f, 1.0f, t);
                    break;
                  }
                }
                blendTexture.SetPixel(x, y, c);
              }
            }
            blendTexture.Apply(false);
            GUI.DrawTexture(area, blendTexture);
          }

          foreach (var tri in triangles) {
            var a = positions[tri.a];
            var b = positions[tri.b];
            var c = positions[tri.c];
            EditorUtils.DrawLine(a, b, 2.0f);
            EditorUtils.DrawLine(b, c, 2.0f);
            EditorUtils.DrawLine(c, a, 2.0f);
          }
          Handles.color = color;

          for (int i = 0; i < positions.Length; i++) {
            var selected = reorderableList.index == i ? true : false;
            DrawPosition(positions[i], selected);
          }
          break;
        case EventType.MouseDown:
          if (area.Contains(evt.mousePosition)) {
            evt.Use();
            GUIUtility.hotControl = controlID;
            GUIUtility.keyboardControl = controlID;
            // closest position
            var clickPosition = evt.mousePosition;
            var distance = 16.0f;
            for (int i = 0; i < positions.Length; i++) {
              var d = Vector2.Distance(clickPosition, positions[i]);
              if (d < distance) {
                distance = d;
                reorderableList.index = i;
              }
            }
          }
          break;
        case EventType.MouseUp:
          if (GUIUtility.hotControl == controlID) {
            evt.Use();
            GUIUtility.hotControl = 0;
            reorderableList.index = -1;
          }
          break;
      }
    }

    static void DrawPosition(Vector2 position, bool selected) {
      var outer_rect = new Rect(-3.5f, -3.5f, 7.0f, 7.0f);
      var inner_rect = new Rect(-2.0f, -2.0f, 4.0f, 4.0f);

      var matrix = Handles.matrix;
      Handles.matrix = Matrix4x4.TRS(position, Quaternion.Euler(0.0f, 0.0f, 45.0f), Vector3.one);
      Handles.DrawSolidRectangleWithOutline(outer_rect, Styles.PositionLineColor, Styles.PositionLineColor);
      Handles.matrix = Matrix4x4.TRS(position, Quaternion.Euler(0.0f, 0.0f, 45.0f), Vector3.one);
      Handles.DrawSolidRectangleWithOutline(inner_rect, selected ? Styles.PositionSelectedColor : Styles.PositionShapeColor, Styles.PositionShapeColor);
      Handles.matrix = matrix;
    }

    // static void DrawAnimation (float val, float min, float max, bool selected, Rect area) {
    //   var top = area.y;
    //   var leftRect = new Rect (min, top, val - min, area.height);
    //   var rightRect = new Rect (val, top, max - val, area.height);
    //   Styles.TriangleLeft.Draw (leftRect, selected, selected, false, false);
    //   Styles.TriangleRight.Draw (rightRect, selected, selected, false, false);
    //   area.height -= 1;
    //   var oldColor = Handles.color;
    //   var newColor = selected ? new Color (1f, 1f, 1f, 0.6f) : new Color (1f, 1f, 1f, 0.4f);
    //   Handles.color = newColor;
    //   if (selected) {
    //     Handles.DrawLine (new Vector3 (val, top, 0), new Vector3 (val, top + area.height, 0));
    //   }
    //   var points = new Vector3[2] { new Vector3 (min, top + area.height, 0f), new Vector3 (val, top, 0f) };
    //   Handles.DrawAAPolyLine (points);
    //   points = new Vector3[2] { new Vector3 (val, top, 0f), new Vector3 (max, top + area.height, 0f) };
    //   Handles.DrawAAPolyLine (points);
    //   Handles.color = oldColor;
    // }
  }
}
