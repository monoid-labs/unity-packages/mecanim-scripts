using UnityEditor;
using UnityEngine;

namespace Monoid.Unity.Animation {

  internal static class EditorUtils {

    public static void DrawLine(Vector2 a, Vector2 b, float width) {
      var line = new Vector3[2] {
        a, b
      };
      Handles.DrawAAPolyLine(width, line);
    }

    public static Rect SliceX(this ref Rect r, float size, float space = 0.0f) {
      var rect = r;
      rect.width = Mathf.Min(rect.width, size);
      r.width -= rect.width;
      r.x += rect.width;
      rect.width = Mathf.Max(0, rect.width - space);
      return rect;
    }
  }

}