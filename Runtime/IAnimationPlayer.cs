namespace Monoid.Unity.Animation {

  public interface IAnimationPlayer {

    AnimationState Animation { get; }
    TransitionState Transition { get; }

    AnimationState Play(int id, float progress = 0.0f, float speed = 1.0f);
    AnimationState PlayInFixedTime(int id, float time = 0.0f, float speed = 1.0f);

    AnimationState CrossFade(int id, float fade, float progress = 0.0f, float speed = 1.0f);
    AnimationState CrossFadeInFixedTime(int id, float fade, float time = 0.0f, float speed = 1.0f);
  }

}