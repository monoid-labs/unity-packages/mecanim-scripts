using System;
using Unity.Collections;
using UnityEngine;
#if UNITY_2019_3_OR_NEWER
using UnityEngine.Animations;
#else
using UnityEngine.Experimental.Animations;
#endif

namespace Monoid.Unity.Animation {

  public struct WorldSpaceLayerMixer : IAnimationJob, IDisposable {

    private NativeArray<AnimationStream> streams;
    private NativeArray<TransformStreamHandle> transforms;
    private NativeArray<byte> masked;
    private NativeArray<float> weights;
    private NativeArray<float> fadeMask;

    public int Layers => streams.Length;

    public float GetLayerWeight(int index) => weights[index];
    public void SetLayerWeight(int index, float weight) => weights[index] = weight;

    public float GetLayerMaskWeight(int index) => fadeMask[index];
    public void SetLayerMaskWeight(int index, float fade) => fadeMask[index] = fade;


    public static WorldSpaceLayerMixer CreateJob(Animator animator, int inputs) {
      var transforms = animator.transform.GetComponentsInChildren<Transform>();
      var n = transforms.Length - 1; // exclude the animator itself

      var job = new WorldSpaceLayerMixer
      {
        streams = new NativeArray<AnimationStream>(inputs, Allocator.Persistent, NativeArrayOptions.UninitializedMemory),
        transforms = new NativeArray<TransformStreamHandle>(n, Allocator.Persistent, NativeArrayOptions.UninitializedMemory),
        masked = new NativeArray<byte>(inputs * n, Allocator.Persistent, NativeArrayOptions.ClearMemory),
        weights = new NativeArray<float>(inputs, Allocator.Persistent, NativeArrayOptions.ClearMemory),
        fadeMask = new NativeArray<float>(inputs, Allocator.Persistent, NativeArrayOptions.ClearMemory),
      };
      for (var i = 0; i < job.transforms.Length; ++i) {
        job.transforms[i] = animator.BindStreamTransform(transforms[i + 1]);
        // Debug.Log($"{TransformPath(transforms[i + 1], animator.transform)}");
      }
      if (inputs > 0) {
        job.weights[0] = 1.0f;
      }
      return job;
    }

    public void SetLayerMaskFromAvatarMask(int input, AvatarMask mask) {
      // Debug.Log(mask.transformCount + " " + transforms.Length);
      for (int i = 0, n = transforms.Length, m = mask.transformCount; i < n; i++) {

        masked[input * n + i] = (byte)((i < m && mask.GetTransformActive(i + 1)) ? 0 : 1);
        // Debug.Log($"{mask.GetTransformPath(i)}:{i<mask.transformCount}:{mask.GetTransformActive(i)}");
      }
    }

    // private static string TransformPath(Transform transform, Transform parent = null) {
    //   if (transform == null || transform == parent) {
    //     return string.Empty;
    //   }
    //   var prefix = TransformPath(transform.parent, parent);
    //   return (prefix.Length > 0)
    //        ? parent.name + "/" + transform.name
    //        : transform.name;
    // }

    public void ProcessAnimation(AnimationStream stream) {
      for (int i = 0, n = streams.Length; i < n; i++) {
        streams[i] = stream.GetInputStream(i);
      }

      for (int i = 0, n = transforms.Length; i < n; i++) {
        var transform = transforms[i];
        // var pos = transform.GetLocalPosition(stream);
        // var rot = transform.GetLocalRotation(stream);
        var pos = transform.GetPosition(stream);
        var rot = transform.GetRotation(stream);
        var scale = transform.GetLocalScale(stream);

        for (int j = 0, m = streams.Length; j < m; j++) {
          if (!streams[j].isValid) {
            continue;
          }
          if (masked[j * n + i] > 0 && fadeMask[j] == 0.0f) {
            continue;
          }
          var w = j == 0 ? 1.0f : weights[j];
          w = (1.0f - fadeMask[j]) * w + fadeMask[j] * 1.0f;

          // var p = transform.GetLocalPosition(streams[j]);
          // var r = transform.GetLocalRotation(streams[j]);
          var p = transform.GetPosition(streams[j]);
          var r = transform.GetRotation(streams[j]);
          var s = transform.GetLocalScale(streams[j]);

          pos = Vector3.Lerp(pos, p, w);
          rot = Quaternion.Slerp(rot, r, w);
          scale = Vector3.Lerp(scale, s, w);
        }

        transform.SetLocalScale(stream, scale);
        transform.SetPosition(stream, pos);
        transform.SetRotation(stream, rot);
        // transform.SetLocalPosition(stream, pos);
        // transform.SetLocalRotation(stream, rot);
      }
    }

    public void ProcessRootMotion(AnimationStream stream) { }

    public void Dispose() {
      streams.Dispose();
      transforms.Dispose();
      weights.Dispose();
      masked.Dispose();
      fadeMask.Dispose();
    }
  }

}