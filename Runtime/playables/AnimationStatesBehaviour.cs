using UnityEngine;
using UnityEngine.Animations;
using UnityEngine.Playables;

namespace Monoid.Unity.Animation {

  public sealed class AnimationStatesBehaviour : PlayableBehaviour, IAnimationPlayer {

    struct PlayAction {
      public int id;
      public float time, speed, fade;
      public bool sync;
    }

    AnimationClip[] clips;

    AnimationMixerPlayable mixer;

    PlayAction action;
    float fadeTime;
    bool reverse;

    TransitionState transition = new TransitionState {
      current = AnimationState.None,
      next = AnimationState.None,
    };

    public bool DefaultPoseBlending { get; set; }

    public int States => clips.Length;

    public float Weight => this.DefaultPoseBlending ? 0.0f : this.mixer.GetOutput(0).GetInputWeight(0);


    public void Initialize(PlayableGraph graph, ScriptPlayable<AnimationStatesBehaviour> playable, AnimationClip[] clips) {
      this.clips = clips;

      this.action.id = -1;
      this.transition = new TransitionState {
        current = AnimationState.None,
        next = AnimationState.None,
      };

      this.mixer = AnimationMixerPlayable.Create(graph);

      playable.ConnectInput(0, mixer, 0, 0.0f);
    }

    #region IAnimationPlayer

    public AnimationState Animation => this.transition.duration <= 0.0f ? this.transition.current : this.transition.next;
    public TransitionState Transition => this.transition;

    public AnimationState Play(int id, float progress = 0.0f, float speed = 1.0f) => CrossFade(id, 0.0f, progress, speed);

    public AnimationState PlayInFixedTime(int id, float time = 0.0f, float speed = 1.0f) => CrossFadeInFixedTime(id, 0.0f, time, speed);

    public AnimationState CrossFade(int id, float fade, float progress = 0.0f, float speed = 1.0f) {
      float duration = Duration(id);
      return CrossFadeInFixedTime(id, duration, fade, progress * duration, speed);
    }
    public AnimationState CrossFadeInFixedTime(int id, float fade, float time = 0.0f, float speed = 1.0f) {
      float duration = Duration(id);
      return CrossFadeInFixedTime(id, duration, fade, time, speed);
    }

    // Make sure duration is always > 0.0 (1.0f as default)
    AnimationState CrossFadeInFixedTime(int id, float duration, float fade, float time, float speed) {
      this.action = new PlayAction { id = id, time = time, speed = speed, fade = fade, sync = true };
      return new AnimationState { id = id, duration = duration, progress = time / duration, speed = speed };
    }

    float Duration(int id) {
      float duration = 1.0f;
      if (id < 0 || id >= this.clips.Length) {
        return duration;
      }
      AnimationClip clip = this.clips[id];
      if (!clip) {
        return duration;
      }
      float length = clip.length;
      if (length > 0.0f) {
        duration = length;
      }
      return duration;
    }

    #endregion

    #region PlayableBehaviour

    public override void PrepareFrame(Playable playable, FrameData info) {
      if (this.clips == null || this.clips.Length <= 0) {
        return;
      }

      // update states
      if (this.action.sync) {
        this.action.sync = false;

        if (this.action.fade <= 0.0f) {
          this.SyncPlay(this.action.id, this.action.time, this.action.speed);
        } else {
          this.SyncCrossFade(this.action.id, this.action.fade, this.action.time, this.action.speed);
        }

        this.fadeTime = 0.0f;
        this.transition.progress = 0.0f;
        this.transition.duration = this.action.fade;
      }


      // update transition
      if (this.transition.duration > 0.0f) { // has transition
        this.transition.progress = this.fadeTime / this.transition.duration;
        this.fadeTime += info.deltaTime * Time.timeScale; // Time.deltaTime;

        if (this.fadeTime >= this.transition.duration) { // transition finished
          this.transition.duration = 0.0f;
          this.FinishTransition();
          if (!this.DefaultPoseBlending) {
            this.mixer.GetOutput(0).SetInputWeight(0, this.mixer.GetInputCount() == 0 ? 0.0f : 1.0f);
          }
        } else { // update  weights
          int inputs = this.mixer.GetInputCount();
          if (inputs > 1) { // cross-face
            float s = 1.0f - this.transition.progress;
            float t = this.transition.progress;
            this.mixer.SetInputWeight(0, s);
            this.mixer.SetInputWeight(1, t);
          } else if (inputs == 1) {
            float t = t = this.transition.progress;
            if (this.reverse) {
              t = 1.0f - t;
            }
            if (this.DefaultPoseBlending) {
              this.mixer.SetInputWeight(0, t);
            } else {
              this.mixer.SetInputWeight(0, 1.0f);
              this.mixer.GetOutput(0).SetInputWeight(0, t);
            }
          }
        }
      } else {
        if (!this.DefaultPoseBlending) {
          this.mixer.GetOutput(0).SetInputWeight(0, this.mixer.GetInputCount() == 0 ? 0.0f : 1.0f);
        }
      }

      { // update state progress
        int inputs = this.mixer.GetInputCount();
        if (inputs > 0) {
          Playable current = this.mixer.GetInput(0);
          this.transition.current.progress = (float)current.GetTime() / this.transition.current.duration;
        }
        if (inputs > 1) {
          Playable next = this.mixer.GetInput(1);
          this.transition.next.progress = (float)next.GetTime() / this.transition.next.duration;
        }
      }
    }

    #endregion

    void SyncPlay(int id, float time, float speed) {
      int inputs = this.mixer.GetInputCount();
      Playable current = inputs > 0 ? this.mixer.GetInput(0) : Playable.Null;
      Playable next = inputs > 1 ? this.mixer.GetInput(1) : Playable.Null;
      if (inputs > 1) { // in-transition
        this.mixer.DisconnectInput(1);
        this.mixer.SetInputCount(inputs = 1);
        next.Destroy();
      }
      if (inputs > 0) { // playback
        if (id == this.transition.current.id) { // optimization if it's the same clip to reduce accumulated clip creation spikes (can be removed with logic change)
          current.SetSpeed(speed);
          current.SetTime(time);
          current.Play();
          this.transition.current.progress = (byte)time / this.clips[id].length;
          this.transition.current.speed = speed;
          return;
        }
        this.transition.current = AnimationState.None;
        this.mixer.DisconnectInput(0);
        this.mixer.SetInputCount(inputs = 0);
        current.Destroy();
      }

      bool valid = (id >= 0 && id < this.clips.Length) && this.clips[id] && this.clips[id].length > 0.0f;
      if (!valid) {
        return;
      }

      AnimationClip clip = this.clips[id];
      current = AnimationClipPlayable.Create(this.mixer.GetGraph(), clip);
      current.SetSpeed(speed);
      current.SetTime(time);
      current.Play();

      float duration = clip.length;
      this.transition.current = new AnimationState {
        id = id,
        duration = duration,
        progress = time / duration,
        speed = speed,
      };

      this.mixer.SetInputCount(inputs = 1);
      this.mixer.ConnectInput(0, current, 0, 1.0f);
    }

    void SyncCrossFade(int id, float fade, float time, float speed) {
      int inputs = this.mixer.GetInputCount();
      if (inputs > 1) { // already in transition -> interrupt
        if (this.mixer.GetInputWeight(0) < this.mixer.GetInputWeight(1)) { // next is dominant: set next as current, clear next
          inputs = this.FinishTransition(); // after this we have 0 or 1 inputs. never 2
        } else {
          Playable next = this.mixer.GetInput(1);
          this.mixer.DisconnectInput(1);
          this.mixer.SetInputCount(inputs = 1);
          next.Destroy();
        }
      }


      inputs = this.mixer.GetInputCount();

      bool valid = (id >= 0 && id < this.clips.Length) && this.clips[id] && this.clips[id].length > 0.0f;
      if (!valid) {
        this.reverse = inputs > 0; // fade out existing
        return; // nothing more todo
      }

      this.reverse = false;

      AnimationClip clip = this.clips[id];
      float duration = clip.length;
      var state = new AnimationState {
        id = id,
        duration = duration,
        progress = time / duration,
        speed = speed,
      };
      var playable = AnimationClipPlayable.Create(this.mixer.GetGraph(), clip);
      playable.SetSpeed(speed);
      playable.SetTime(time);
      playable.Play();


      if (inputs <= 0) { // fade in (single)
        this.transition.current = state;
        this.mixer.SetInputCount(inputs = 1);
        this.mixer.ConnectInput(0, playable, 0, 0.0f);
        return;
      }

      // cross-fade
      this.transition.next = state;
      this.mixer.SetInputCount(inputs = 2);
      this.mixer.ConnectInput(1, playable, 0, 0.0f);
    }

    int FinishTransition() {
      int inputs = this.mixer.GetInputCount();

      if (inputs <= 0) {
        return inputs; // nothing todo
      }

      if (inputs <= 1) {
        if (!reverse) { // fade-in finished
          this.mixer.SetInputWeight(0, 1.0f);
          return inputs;
        }
        // fade-out finished
        this.transition.current = AnimationState.None;
        Playable current = this.mixer.GetInput(0);
        this.mixer.DisconnectInput(0);
        this.mixer.SetInputCount(inputs = 0);
        current.Destroy();
        return inputs;
      }

      { // inputs > 1 -> cross fade finished
        this.transition.current = this.transition.next;
        this.transition.next = AnimationState.None;

        Playable current = this.mixer.GetInput(0);
        Playable next = this.mixer.GetInput(1);

        this.mixer.DisconnectInput(1);
        this.mixer.DisconnectInput(0);

        current.Destroy();
        current = next;
        next = Playable.Null;

        this.mixer.SetInputCount(inputs = 1);
        this.mixer.ConnectInput(0, current, 0, 1.0f);
        return inputs;
      }
    }

  }
}
