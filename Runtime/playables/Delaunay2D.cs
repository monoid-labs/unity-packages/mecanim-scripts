using System.Collections.Generic;
using UnityEngine;

namespace Monoid.Unity.Animation {

  internal static class Delaunay2D {

    public struct Triangle {
      public int a, b, c;
    }

    public static int Triangulate(Vector2[] points, List<Triangle> triangles) {
      var min = Vector2.one * float.PositiveInfinity;
      var max = Vector2.one * float.NegativeInfinity;
      foreach (var p in points) {
        min = Vector2.Min(min, p);
        max = Vector2.Max(max, p);
      }
      var center = 0.5f * (min + max);
      var size = max - min;
      var r = Mathf.Max(size.x, size.y);

      var tri_bounds = new Vector2[3] {
        new Vector2 (center.x - 20.0f * r, center.y - r),
        new Vector2 (center.x, center.y + 20.0f * r),
        new Vector2 (center.x + 20.0f * r, center.y - r),
      };

      var start = triangles.Count;
      triangles.Add(new Triangle { a = -3, b = -2, c = -1 });

      var polygon = new List<Edge>();
      for (int i = 0; i < points.Length; i++) {
        polygon.Clear();

        for (int j = start; j < triangles.Count; j++) {
          var tri = triangles[j];
          var a = tri.a >= 0 ? points[tri.a] : tri_bounds[3 + tri.a];
          var b = tri.b >= 0 ? points[tri.b] : tri_bounds[3 + tri.b];
          var c = tri.c >= 0 ? points[tri.c] : tri_bounds[3 + tri.c];

          if (InsideCircleCircum(a, b, c, points[i])) {
            triangles[j--] = triangles[triangles.Count - 1];
            triangles.RemoveAt(triangles.Count - 1);
            polygon.Add(new Edge { from = tri.a, to = tri.b });
            polygon.Add(new Edge { from = tri.b, to = tri.c });
            polygon.Add(new Edge { from = tri.c, to = tri.a });
          }
        }

        for (int j = 0; j < polygon.Count; j++) {
          var remove = false;
          for (int k = j + 1; k < polygon.Count; k++) {
            if (AreEqual(polygon[j], polygon[k])) {
              remove = true;
              polygon[k--] = polygon[polygon.Count - 1];
              polygon.RemoveAt(polygon.Count - 1);
            }
          }
          if (remove) {
            polygon[j--] = polygon[polygon.Count - 1];
            polygon.RemoveAt(polygon.Count - 1);
          }
        }

        for (int j = 0; j < polygon.Count; j++) {
          triangles.Add(new Triangle { a = polygon[j].from, b = polygon[j].to, c = i });
        }
      }

      var count = 0;
      for (int i = start; i < triangles.Count; i++) {
        if (IsValid(triangles[i])) {
          count++;
          continue;
        }
        triangles[i--] = triangles[triangles.Count - 1];
        triangles.RemoveAt(triangles.Count - 1);
      }
      return triangles.Count - start;
    }
    struct Edge {
      public int from, to;
    }

    static bool InsideCircleCircum(Vector2 a, Vector2 b, Vector2 c, Vector2 vertex) {

      var aa = a.x * a.x + a.y * a.y;
      var bb = b.x * b.x + b.y * b.y;
      var cc = c.x * c.x + c.y * c.y;

      var circum = new Vector2(
        (aa * (c.y - b.y) + bb * (a.y - c.y) + cc * (b.y - a.y)) / (a.x * (c.y - b.y) + b.x * (a.y - c.y) + c.x * (b.y - a.y)),
        (aa * (c.x - b.x) + bb * (a.x - c.x) + cc * (b.x - a.x)) / (a.y * (c.x - b.x) + b.y * (a.x - c.x) + c.y * (b.x - a.x)));

      circum *= 0.5f;
      var r = (a - circum).sqrMagnitude;
      var d = (vertex - circum).sqrMagnitude;
      return d <= r;
    }

    static bool AreEqual(Edge a, Edge b) {
      return (a.from == b.from && a.to == b.to) || (a.from == b.to && a.to == b.from);
    }

    static bool IsValid(Triangle t) {
      return t.a >= 0 && t.b >= 0 && t.c >= 0;
    }
  }

}