using System;
using UnityEngine;
using UnityEngine.Playables;

namespace Monoid.Unity.Animation {

  [CreateAssetMenu(fileName = "BlendSpace2D", menuName = "Playables/Animation/BlendSpace2D")]//, order = Package.MenuOrder)]
  public class BlendSpace2D : PlayableAsset {

    [Serializable]
    public struct State {
      public AnimationClip clip;
      public Vector2 position;
      public float speed;
    }

    public State[] states = { };
    public bool interpolate;

    public override Playable CreatePlayable(PlayableGraph graph, GameObject owner) {
      return CreatePlayable(in graph, owner, states, interpolate);
    }

    public static BlendSpaceBehaviour2D GetBehaviour(in Playable playable) {
      var script = (ScriptPlayable<BlendSpaceBehaviour2D>)playable;
      return script.GetBehaviour();
    }

    public static Playable CreatePlayable(in PlayableGraph graph, GameObject owner, State[] states, bool interpolate) {
      var n = states.Length;
      var clips = new AnimationClip[n];
      var positions = new Vector2[n];
      var speeds = new float[n];
      for (int i = 0; i < n; i++) {
        clips[i] = states[i].clip;
        positions[i] = states[i].position;
        speeds[i] = states[i].speed;
      }

      var playable = ScriptPlayable<BlendSpaceBehaviour2D>.Create(graph, 1);
      playable.GetBehaviour().Initialize(graph, playable, clips, positions, speeds, interpolate);
      return playable;
    }

  }
}
