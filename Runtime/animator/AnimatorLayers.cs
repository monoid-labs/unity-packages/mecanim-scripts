using System.Runtime.CompilerServices;
using UnityEngine;

namespace Monoid.Unity.Animation {

  public sealed class AnimatorLayers {

    private readonly Animator controller;
    private readonly AnimatorStates[] layers;

    //-------------------------------------------------------------------------

    public int Count {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get { return layers.Length; }
    }

    public AnimatorStates this[int i] {
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      get { return layers[i]; }
    }

    //-------------------------------------------------------------------------

    public AnimatorLayers(Animator animator) {
      controller = animator;
      layers = new AnimatorStates[controller.layerCount];
      Refresh();
    }

    public void Refresh() {
      for (int i = 0; i < layers.Length; i++) {
        layers[i].Refresh(controller, i);
      }
    }

    //-------------------------------------------------------------------------

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public bool TransitionTo(int layer, int state, float normalizedTransitionDuration, float normalizedTimeOffset) {
      var states = layers[layer];
      if (states.AnyName(state)) {
        return false;
      }
      if (normalizedTransitionDuration <= 0.0f) {
        controller.Play(state, layer, normalizedTimeOffset);
      } else {
        controller.CrossFade(state, normalizedTransitionDuration, layer, normalizedTimeOffset);
      }
      return true;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public bool TransitionTo(int layer, string state, float normalizedTransitionDuration, float normalizedTimeOffset) {
      var states = layers[layer];
      if (states.AnyName(state)) {
        return false;
      }
      if (normalizedTransitionDuration <= 0.0f) {
        controller.Play(state, layer, normalizedTimeOffset);
      } else {
        controller.CrossFade(state, normalizedTransitionDuration, layer, normalizedTimeOffset);
      }
      return true;
    }

    //-------------------------------------------------------------------------

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public bool FixedTransitionTo(int layer, int state, float fixedTransitionDuration, float fixedTimeOffset) {
      var states = layers[layer];
      if (states.AnyName(state)) {
        return false;
      }
      if (fixedTransitionDuration <= 0.0f) {
        controller.PlayInFixedTime(state, layer, fixedTimeOffset);
      } else {
        controller.CrossFadeInFixedTime(state, fixedTransitionDuration, layer, fixedTimeOffset);
      }
      return true;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public bool FixedTransitionTo(int layer, string state, float fixedTransitionDuration, float fixedTimeOffset) {
      var states = layers[layer];
      if (states.AnyName(state)) {
        return false;
      }
      if (fixedTransitionDuration <= 0.0f) {
        controller.PlayInFixedTime(state, layer, fixedTimeOffset);
      } else {
        controller.CrossFadeInFixedTime(state, fixedTransitionDuration, layer, fixedTimeOffset);
      }
      return true;
    }

    //-------------------------------------------------------------------------
  }

}