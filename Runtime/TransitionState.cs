using System;

namespace Monoid.Unity.Animation {

  [Serializable]
  public struct TransitionState {
    public float progress; // normalized time
    public float duration; // if speed == 1.0
    public AnimationState current, next;
  }


  public static class TransitionStateExtensions {
    public static bool IsActive(in this TransitionState state) => (state.duration > 0.0f);
  }

}